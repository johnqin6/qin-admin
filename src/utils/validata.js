/**
 * 公共验证方法文件
 * 1, isvalidUsername(str) 验证用户名是否存在  参数：str,  思路：数组的indexOf, 字符串trim()
 * 2, validateURL(url) 验证url是否合法 参数：url,  reg.text(url); 
 * 3, validateLowerCase(str) 验证是否小写 参数：str,  reg.text(str)
 * 4, validateUpperCase(str) 验证是否大写 参数：str,  reg.text(str)
 * 5, validatAlphabets(str) 验证是否大小写
 */

 //验证用户名是否存在
 export function isvalidUsername(str) {
     let valid_map = ['admin','editor'];
     return valid_map.indexOf(str.trim()) > 0;
 }

 //验证url是否合法
 export function validateURL(url) {
     let reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
     return reg.test(url);
 }

 //验证是否大写 
 export function validateUpperCase(str) {
    let reg = /^[A-Z]$/;
    return reg.test(str);
 }

 //验证是否小写 
 export function validateLowerCase(str) {
    let reg = /^[a-z]$/;
    return reg.test(str);
 }

 //验证是否大小写
export function validatAlphabets(str) {
    let reg = /^[a-zA-Z]&/;
    return reg.test(str);
}

/**
 * validatenull(val) 验证是否为空 判断是否是布尔值，是否是数组，是否是对象
 * isPhone(phone) 验证是否是手机号
 */
//验证是否为空
export function validatenull(val) {
    if(typeof val === 'boolean'){
        return false;
    }
    if(val instanceof Array){
        if(val.length === 0) return true;
    }else if(val instanceof Object){
        if(JSON.stringify(val) === '{}') return true;
    }else{
        if(val === null || val === 'undefined' || val === undefined || val === '') return true;
        return false;
    }
    return false;
}

//验证是否是手机号码或座机
export function isPhone(phone){
    let isphone = /^0\d{2,3}-?\d{7,8}$/;
    let ismobile = /^1[3|4|5|7|8]\d{9}$/;
    if(isphone.test(phone) || ismobile.test(phone)){
        return true;
    }
    return false;
}