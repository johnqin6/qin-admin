
/**
 * 函数名称param2Obj（普通暴露）, 作用：解析地址url
 * 思路：url拆分数组(split('?'))获取域名后的参数-> 判断搜索参数(search)是否存在 -> 存在，返回拆解的参数
 *  拆解的参数：解码(decodeURLComponent()),
 *             正则替换(replace(正则，替换字符)) ",&,= -> \", ",", ":" -> 
 *             json解析JSON.parse()
 * @param {*} url 
 */
export function param2Obj(url){
    let search = url.split('?')[1];
    if(!search){
        return {}
    }
    return JSON.parse(decodeURIComponent(search).replace(/"/g,'\\"').replace(/&/,'","').replace(/=/,'"="'));
}