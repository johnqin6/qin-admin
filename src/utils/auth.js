/**
 * 引入Cookies(依赖 js-cookie)
 * 定义常量 TokenKey = "Admin-Token"
 * 普通暴露三个方法
 * 1, getToken() 返回 得到的cookie(Cookies.get())
 * 2, setToken(token) 参数：token; 返回 执行建立cookie(Cookies.set(TokenKey, token))
 * 3, removeToken() 返回 执行删除cookie（Cookies.remove(TokenKey)）
 */

 import Cookies from 'js-cookie';

 const TokenKey = 'Admin-Token';

 //得到token
 export function getToken() {
     return Cookies.get(TokenKey);
 }

 //建立token
 export function setToken(token) {
    return Cookies.set(TokenKey, token);
 }

 //删除token
 export function removeToken(){
    return Cookies.remove(TokenKey);
 }