/**
 *  引入request (@/utils/request)
 *  普通暴露三个函数
 *  1, login(username,password) 返回封装的request请求方法(post)
 *  2, getInfo(token) 返回封装的request请求方法(get)
 *  3, logout() 返回封装的request请求方法(post)
 */
import request from "@/utils/request";

/**
 * 请求登录
 * @param {*} username 
 * @param {*} password 
 */
export function login(username, password) {
    return request({
        url: 'user/login',
        method: 'post',
        data: {
            username,
            password
        }
    });
};

export function getInfo(token){
    return request({
        url: 'user/info',
        method: 'get',
        params: {
            token
        }
    });
}

export function logout(){
    return request({
        url: 'user/logout',
        method: 'post'
    });
}

