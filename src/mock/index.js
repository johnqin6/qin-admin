import Mock from 'mockjs';
import userApi from './users';
import loginApi from './login';
// 设置全局延时 没有延时的话有时候会检测不到数据变化 建议保留
Mock.setup({
    timeout: '300-600'
});

Mock.mock(/\/user\/getUser/, 'get', userApi.getUsersData);

//登陆相关
Mock.mock(/\/user\/login/,'post',loginApi.loginByUsername);
Mock.mock(/\/user\/info/,'get',loginApi.getUserInfo);
Mock.mock(/\/user\/logout/,'post',loginApi.logout);

export default Mock;