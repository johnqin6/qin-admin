import { param2Obj } from '@/utils';

const userMap = {
    admin: {
        code: 0,
        roles: ['admin'],
        token: 'admin',
        introduction: '超级管理员',
        avatar: 'logo.png',
        name: 'Super Admin'
    },
    editor: {
        code: 0,
        roles: ['edit'],
        token: 'edit',
        introduction: '普通用户',
        avatar: 'logo.png',
        name: 'normal editor'
    }
}

/*
 *默认暴露  
 * 三个函数：1，获取登陆用户名称，返回相应数据（loginByUsername） 
 *          2, 获取用户信息(token)，返回用户数据（getUserInfo）需要param2Obj方法对url进行解析 来自util文件夹的工具函数
 *          3, 退出系统（logout）返回成功信息
*/
export default {
    loginByUsername: config => {
        let { username } = JSON.parse(config.body);
        return userMap[username];
    },
    getUserInfo: config => {
        let { token } = JSON.parse(config.url);
        return userMap[token];
    },
    logout: () => {
        return {
            code: 200,
            message: 'success'
        }
    }
}