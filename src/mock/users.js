import Mock from 'mockjs';
const Random = Mock.Random;

let users = [];

for(let i = 0; i < 1000; i++){
    users.push({
        id: Random.guid(),
        name: Random.cname(),
        phone: Random.natural(12000000000,20000000000),
        province: Random.province(),
        city: Random.city(),
        county: Random.county(),
        address: '初始地点',
        avatar: Random.image('200x100'),
        img: Random.dataImage('200x100','hello')
    });
}

export default {
    getUsersData(){
        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         console.log(users);
        //         resolve([200,{code: 200, users: users}]);
        //     },100)
        // });
        return users;
    }
};