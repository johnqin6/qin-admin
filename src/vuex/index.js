import Vue from 'vue';
import Vuex from 'vuex';
import { getUsersData } from "@/api/user.js";
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0,
        user: {
            name: '',
            phone: ''
        }
    },
    getters: {
        countAdd(state){
            return state.count++;
        }
    },
    mutations: {
        addCount(state,payload){
            state.count += payload.n;
        }
    },
    actions: {
        changeCount(context,payload){
            console.log(context,payload);
        },
        async getUser(context){
            let res = await getUsersData();
            let user = res.status === 200 ? res.data[0] : '';
            return new Promise((resolve, reject) => {
                if(user){
                    resolve({code: 200, user});
                }
            });
        },
        // getUser(context){
        //      return new Promise((resolve, reject) => {
        //         getUsersData().then(res => {
        //            if(res){
        //               resolve({code: 200, res});
        //            }
        //         });
        //      })
        // }
        
    }
});

export default store;