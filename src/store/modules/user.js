import { login, logout, getInfo } from '@/api/login';
import { getToken, setToken, removeToken } from '@/utils/auth';

const user = {
    state: {
        rolus: [],
        token: getToken(),
        avatar: '',
        name: '',
    },
    mutations: {

    },
    actions: {

    }
};

export default user;